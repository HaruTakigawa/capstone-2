﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBase : MonoBehaviour
{
    public enum aimType
    {
        Ground,
        Air,
        both
    }
    public Transform firePoint;
    public string towerName;
    public aimType targetType;

    public float damage;
    public float range;
    public float reloadTime;
    public float splashRadius;
    public int goldCost;
    public int energy;
    public GameObject bulletPrefab;
    public List<GameObject> enemiesList;
    protected float reload;
    protected GameObject target;
    public virtual void Start()
    {
        reload = reloadTime;
    }
    public virtual void GetTargets()
    {
        // Basic Target acquisition. 
        // Gets nearest enemy from the list
        // Then sets that to its primary target
        GameObject nearestEnemy = null;
        float shortestDistance = Mathf.Infinity;
        if (enemiesList.Count <= 0) return;
        foreach (GameObject enemy in enemiesList)
        {
            float distanceToEnemy = Vector2.Distance(this.transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy;
        }
        else
        {
            target = null;
        }
    }
    public virtual void Shoot()
    {
        reloadTime -= Time.deltaTime;
        if (reloadTime <= 0f) reloadTime = 0f;
        if (target == null) return;

        if (reloadTime <= 0f)
        {
            //Fire
            GameObject bullet = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            bullet.GetComponent<BulletBase>().tower = this;
            if (bullet != null)
            {
                bullet.GetComponent<BulletBase>().Seek(target);
            }
            reloadTime = reload;
            print("Shoot");
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
