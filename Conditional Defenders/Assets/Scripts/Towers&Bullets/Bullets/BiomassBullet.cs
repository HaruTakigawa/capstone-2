﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomassBullet : BulletBase
{

    public override void Update()
    {
        base.Update();
    }
    public override void HitTarget()
    {
        //tower.splashRadius

        Explode();
        Destroy(gameObject);
    }
    void Explode()
    {
        if (!target.GetComponent<Poison>())
        {
            target.AddComponent<Poison>();
        }
        else
        {
            target.GetComponent<Poison>().timer = 0;
        }
        DamageEnemy(target.gameObject);
        Collider[] colliders = Physics.OverlapSphere(transform.position, tower.splashRadius);
        foreach (Collider col in colliders)
        {
            if (!col.GetComponent<UnitsBase>()) return;
            Poison poison = col.gameObject.GetComponent<Poison>();
            if (poison == null)
            {
                col.gameObject.AddComponent<Poison>();
            }
            else
            {
                poison.timer = 0;
            }
            DamageEnemy(col.gameObject);
        }
        print("destroyed");
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, tower.splashRadius);
    }
}
