﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FusionBullet : BulletBase
{
    public int bounceAmount;
    private List<GameObject> damagedTarget;
    public override void Update()
    {
        base.Update();
    }
    public override void HitTarget()
    {
        for (int i = 0; i < bounceAmount; i++)
        {
            if (target == null) return;
            DamageEnemy(tower.enemiesList[i]);
            target = tower.enemiesList[i + 1];
        }
        Destroy(gameObject);
    }

}
