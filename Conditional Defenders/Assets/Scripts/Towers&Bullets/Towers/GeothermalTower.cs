﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeothermalTower : TowerBase
{
    public List<GameObject> targetsList;
    public override void Start()
    {
        base.Start();
    }
    void Update()
    {
        GetTargets();
        Shoot();
    }
    public override void GetTargets()
    {
        foreach (GameObject enemy in enemiesList)
        {
            // if enemy distance from tower is less than the range i add it to target list
            // then i do it for all of em and check if theres a repeating then ill skip that

            float distanceToEnemy = Vector2.Distance(this.transform.position, enemy.transform.position);
            if (distanceToEnemy < range)
            {
                if (!targetsList.Exists(ind => ind.Equals(enemy)))
                {
                    targetsList.Add(enemy);
                }
            }
            if (distanceToEnemy > range || enemy.GetComponent<UnitsBase>().health <= 0)
            {
                targetsList.Remove(enemy);
            }
        }
    }
    public override void Shoot()
    {
        reloadTime -= Time.deltaTime;
        if (reloadTime <= 0f) reloadTime = 0f;
        if (targetsList == null) return;
        if (reloadTime <= 0f)
        {
            //Fire   
            foreach (GameObject target in targetsList)
            {
                // damage 
                if (target.GetComponent<UnitsBase>().health <= 0)
                {
                    SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Remove(target);
                }
                else
                {
                    target.GetComponent<UnitsBase>().TakeDamage(damage);
                }

            }
            reloadTime = reload;
        }
    }
}
