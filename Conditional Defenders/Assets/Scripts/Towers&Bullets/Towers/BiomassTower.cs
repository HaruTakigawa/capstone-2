﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomassTower : TowerBase
{
    public override void Start()
    {
        base.Start();
    }
    void Update()
    {
        GetTargets();
        Shoot();
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Shoot()
    {
        base.Shoot();
    }
}
