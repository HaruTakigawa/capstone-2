﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindTower : TowerBase
{
    public List<GameObject> targetsList;
    void Update()
    {
        GetTargets();

        Shoot();
    }
    public override void GetTargets()
    {
        foreach (GameObject enemy in enemiesList)
        {
            // if enemy distance from tower is less than the range i add it to target list
            // then i do it for all of em and check if theres a repeating then ill skip that
            float distanceToEnemy = Vector2.Distance(this.transform.position, enemy.transform.position);
            if (distanceToEnemy < range)
            {
                if (!targetsList.Exists(ind => ind.Equals(enemy)))
                {
                    targetsList.Add(enemy);
                }
            }
            if (distanceToEnemy > range || enemy.GetComponent<UnitsBase>().health <= 0)
            {
                targetsList.Remove(enemy);
            }
        }
    }
    public override void Shoot()
    {
        if (targetsList == null) return;

        foreach (GameObject target in targetsList)
        {
            if (target.GetComponent<UnitsBase>().health <= 0)
            {
                targetsList.Remove(target);
            }
            //target.GetComponent<UnitsBase>().TakeDamage(damage);
            Slow slow = target.GetComponent<Slow>();
            if (slow == null)
            {
                target.AddComponent<Slow>();
            }
            else if (slow != null)
            {
                slow.timer = 0;
            }
        }
    }
}
