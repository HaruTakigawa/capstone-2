﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleRespawn : MonoBehaviour
{
    public float respawnTimer;
    public List<GameObject> tentacle;
    //public List<Tentacles> tentacleSelective;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RespawnTentacle());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator RespawnTentacle()
    {
        while (true)
        {
            for (int i = 0; i < tentacle.Count; i++)
            {
                yield return new WaitForSeconds(respawnTimer);
                tentacle[i].SetActive(true);
            }

            //if (tentacleSelective[0].isActive == false)
            //{
            //    yield return new WaitForSeconds(respawnTimer);
            //    tentacle[0].SetActive(true);
            //}

            //if (tentacleSelective[1].isActive == false)
            //{
            //    yield return new WaitForSeconds(respawnTimer);
            //    tentacle[1].SetActive(true);
            //}

            //if (tentacleSelective[2].isActive == false)
            //{
            //    yield return new WaitForSeconds(respawnTimer);
            //    tentacle[2].SetActive(true);
            //}

            //if (tentacleSelective[3].isActive == false)
            //{
            //    yield return new WaitForSeconds(respawnTimer);
            //    tentacle[3].SetActive(true);
            //}
        }
    }
}
