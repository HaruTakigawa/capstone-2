﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointAndShoot : MonoBehaviour
{
    public float maxAmmo;
    public float currentAmmo;
    public float reloadTime;
    public bool isReloading = false;
    public bool isShooting = false;

    private Vector3 target;
    public GameObject crosshair;
    public GameObject muzzleFlash;
    public Boss boss;
    public Crosshair ch;

    public List<Health> health;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        muzzleFlash.SetActive(false);
        currentAmmo = maxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = false;
        target = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        crosshair.transform.position = new Vector2(target.x, target.y);

        if (isReloading)
        {
            return;
        }

        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
            if (isShooting == true)
            {
                muzzleFlash.SetActive(true);
            }

            else
            {
                muzzleFlash.SetActive(false);
            }
        }

        if (Input.GetKey(KeyCode.R))
        {
            if (currentAmmo < maxAmmo)
            {
                StartCoroutine(Reload());
                return;
            }
        }

    }

    void Shoot()
    {
        isShooting = true;
        currentAmmo--;
        if (ch.isOnBoss == true)

        {
            boss.health.currentHealth -= 1;

            if (ch.isOnWeakpoint1 == true)
            {
                health[0].currentHealth -= 1;
                boss.weakPointHealth.currentHealth -= 1;
            }

            if (ch.isOnWeakpoint2 == true)
            {
                health[1].currentHealth -= 1;
                boss.weakPointHealth.currentHealth -= 1;
            }

            if (ch.isOnWeakpoint3 == true)
            {
                health[2].currentHealth -= 1;
                boss.weakPointHealth.currentHealth -= 1;
            }

            if (ch.isOnWeakpoint4 == true)
            {
                health[3].currentHealth -= 1;
                boss.weakPointHealth.currentHealth -= 1;
            }
        }
    }

    IEnumerator Reload()
    {
        isReloading = true;
        Debug.Log("Reloading");

        yield return new WaitForSeconds(reloadTime);

        currentAmmo = maxAmmo;
        isReloading = false;
    }
}
