﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tentacles : MonoBehaviour
{
    public Health wpHealth;
    public Health tentacleHealth;
    public GameObject tentacle;
    public bool isActive;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (tentacleHealth.currentHealth <= 0)
        {
            tentacle.SetActive(false);
            tentacleHealth.currentHealth = tentacleHealth.maxHealth;
            isActive = false;
        }

        else
        {
            isActive = true;
        }
    }
}
