﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
public class Timer : MonoBehaviour
{
    public float currentSeconds;
    public float resetTime;
    public Text timerText;
    public Boss boss;
    public Health playerHP;

    public UnityEvent onZero;

    // Start is called before the first frame update
    void Start()
    {
        resetTime = currentSeconds;
    }

    // Update is called once per frame
    void Update()
    {
        timerText.text = "Time until it can attack: " + currentSeconds.ToString("F0");

        currentSeconds -= 1 * Time.deltaTime;

        if (boss.weakPointHealth.currentHealth == 0)
        {
            currentSeconds = resetTime;
        }

        if (currentSeconds <= 0)
        {
            onZero.Invoke();
            currentSeconds = 0;
            playerHP.currentHealth -= 1;
            // Time interval where the player can attack the tentacles to prevent damage. 
            if (boss.weakPointHealth.currentHealth > 0)
            {
                currentSeconds = resetTime;
                //print("Damage Taken");  
            }
        }

        if (boss.health.currentHealth <= 0)
        {
            SceneManager.LoadScene("WinScene");
        }

        if (playerHP.currentHealth == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }


}
