﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject EnemyType;
    private GameObject SpawnedEnemy;
    public float SpawnDelay;
    public float SpawnRate;
    public int InitialTime;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawn", SpawnDelay, SpawnRate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawn()
    {
        SpawnedEnemy = Instantiate(EnemyType, transform.position, Quaternion.identity);


        Health health = SpawnedEnemy.GetComponent<Health>();
    }
}
