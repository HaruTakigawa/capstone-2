﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    public Transform detectorstart, detectorend;
    public bool isOnBoss;
    public bool isOnWeakpoint1;
    public bool isOnWeakpoint2;
    public bool isOnWeakpoint3;
    public bool isOnWeakpoint4;
    public Vector3 target;
    // Update is called once per frame
    void Update()
    {
        //target = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        Raycasting();
        Behaviours();
    }

    void Raycasting()
    {
        //detector = Physics2D.Raycast(Vector2.zero, new Vector2(target.x, target.y));
        Debug.DrawLine(detectorstart.position, detectorend.position, Color.red);
        isOnBoss = Physics2D.Linecast(detectorstart.position, detectorend.position, 1 << LayerMask.NameToLayer("Boss"));
        isOnWeakpoint1 = Physics2D.Linecast(detectorstart.position, detectorend.position, 1 << LayerMask.NameToLayer("Tentacle1"));
        isOnWeakpoint2 = Physics2D.Linecast(detectorstart.position, detectorend.position, 1 << LayerMask.NameToLayer("Tentacle2"));
        isOnWeakpoint3 = Physics2D.Linecast(detectorstart.position, detectorend.position, 1 << LayerMask.NameToLayer("Tentacle3"));
        isOnWeakpoint4 = Physics2D.Linecast(detectorstart.position, detectorend.position, 1 << LayerMask.NameToLayer("Tentacle4"));
    }

    void Behaviours()
    {

    }
}
