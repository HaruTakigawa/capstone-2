﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakpointMovement : MonoBehaviour
{
    public GameObject Weakpoint;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoveWeakpoint());
    }

    // Update is called once per frame
    void Update()
    {
        //MoveWeakpoint();
    }

    IEnumerator MoveWeakpoint()
    {
        while (true)
        {
            Weakpoint.transform.position = transform.GetChild(Random.Range(0, 5)).position;
            yield return new WaitForSeconds(1f);
        }

    }
}
