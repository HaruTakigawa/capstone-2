﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGold : MonoBehaviour
{
    public Health coalHealth;
    public Health naturalHealth;
    public Health oilHealth;
    public Health plasticHealth;
    public Health smokeHealth;
    public GameController controller;
    public int GoldDrop;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (coalHealth.currentHealth <= 0)
        {
            controller.gold += GoldDrop;
        }

        if (naturalHealth.currentHealth <= 0)
        {
            controller.gold += GoldDrop;
        }

        if (oilHealth.currentHealth <= 0)
        {
            controller.gold += GoldDrop;
        }

        if (plasticHealth.currentHealth <= 0)
        {
            controller.gold += GoldDrop;
        }

        if (smokeHealth.currentHealth <= 0)
        {
            controller.gold += GoldDrop;
        }
    }
}
