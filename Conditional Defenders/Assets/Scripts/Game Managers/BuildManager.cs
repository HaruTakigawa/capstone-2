﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public int maxEnergyBank;
    public int curEnergyBank;
    public List<GameObject> towerList;
    public static BuildManager buildManagerInstance;
    private GameObject turretToBuild;
    void Awake()
    {
        if (buildManagerInstance != null)
        {
            print("More than 1 build manager instance");
        }
        buildManagerInstance = this;
    }

    public GameObject GetTurretToBuild()
    {
        return turretToBuild;
    }
    public void SetTurretToBuild(GameObject Turret)
    {
        turretToBuild = Turret;
    }

}
