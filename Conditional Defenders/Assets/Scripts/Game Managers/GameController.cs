﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public float currentPollution;
    public int gold = 500;
    public float maxPollution;
    public float addedPollution;
    public static GameController gameControllerInstance;
    void Awake()
    {
        if (gameControllerInstance != null)
        {
            print("More than 1 Game Controller instance");
        }
        gameControllerInstance = this;
    }

    void Update()
    {
        if (BuildManager.buildManagerInstance.towerList.Count <= 0) return;
        for (int i = 0; i < BuildManager.buildManagerInstance.towerList.Count; i++)
        {
            BuildManager.buildManagerInstance.towerList[i].GetComponent<TowerBase>().enemiesList = SpawnerManager.spawnerManagerInstance.spawnedEnemiesList;
        }
        //for (int i= 0; i < BuildManager.buildManagerInstance.towerList.Count <= 0; i++)
        //{
        //    SpawnerManager.spawnerManagerInstance
        //}
    }
}
