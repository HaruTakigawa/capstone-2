﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildUnit : MonoBehaviour
{
    public List<GameObject> unitsList;
    public GameObject friendlyUnit;
    public Transform factoryLocation;
    public Vector3 spawnOffset;
    private GameObject unitTemp;
    SpawnerManager spawnerManager;
    BuildManager buildManager;
    GameController gameController;
    private void Start()
    {
        spawnerManager = SpawnerManager.spawnerManagerInstance;
        buildManager = BuildManager.buildManagerInstance;
        gameController = GameController.gameControllerInstance;
    }
    public void setUnit(string unitName)
    {
        if (unitName == "Glass")
            friendlyUnit = unitsList[0];
        if (unitName == "Paper")
            friendlyUnit = unitsList[1];
        if (unitName == "Plastic")
            friendlyUnit = unitsList[2];
    }
    public void CreateUnit(string UnitName)
    {
        setUnit(UnitName);
        if (buildManager.curEnergyBank < buildManager.maxEnergyBank && gameController.gold >= friendlyUnit.GetComponent<UnitsBase>().goldCost)
        {            
            unitTemp = Instantiate(friendlyUnit, factoryLocation.position + spawnOffset, factoryLocation.rotation);
            spawnerManager.spawnedFriendlyList.Add(unitTemp);
            buildManager.curEnergyBank += unitTemp.GetComponent<UnitsBase>().energyCost;
            gameController.gold -= unitTemp.GetComponent<UnitsBase>().goldCost;
        }
        else
        {
            print("Not enough energy or not enough gold");
        }
    }
    /*
     * Theres 2 ways i can do this one is instantiate a temp Unit
     * that i can drag around the map. Then "drop" it at a location
     * pros:
     * i have done something like this in the past
     * Cons:
     * It will be quite annoying, to create and drop a unit 1 by 1
     * 
     * or
     * 
     * spawns a unit then i will have a "rally Point" where the units will
     * go to
     * Pros:
     * easier to implement since all i have to do is spawn the unit then tell it to go to the rally point
     * Cons:
     * can be buggy since all of the units will go to 1 position and they
     * will collide with each other
     * To fix this it will involve AI since each unit will have to check 
     * if they are "close" to each other
     * 
     * 
    */
}
