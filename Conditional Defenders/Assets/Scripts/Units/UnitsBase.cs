﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitsBase : MonoBehaviour
{
    public enum UnitType
    {
        Ground,
        Air
    }
    public enum TargetType
    {
        Ground,
        Air,
        Both
    }
    public enum UnitState
    {
        Attacking,
        Moving,
        Idle
    }
    GameController gameController;
    SpawnerManager spawnerManager;
    private GameObject target;
    public string unitName;
    public UnitState unitState;
    public UnitType unitType;
    public TargetType targetType;

    public int goldAmount;
    public int goldCost;
    public int energyCost;
    public float health;
    public float damage;
    public float attackRate;
    public float movementSpeed;
    public float detectionRange;
    public Vector3 attackStateOffset;
    public List<GameObject> opponentList;
    private float direction;
    private float tempAttackRate;
    //public List<GameObject> targetList;
    public void TakeDamage(float damage)
    {


        health -= damage;

    }
    public virtual void GoToTarget()
    {
        // Need to change
        // Need to check what the target is
        // Need to check Attack type before actually attacking
        // Do the same with towers
        if (target == null) return;
        Vector2 dir = (target.transform.position + new Vector3((attackStateOffset.x * direction), attackStateOffset.y, attackStateOffset.z)) - transform.position;
        float distanceThisFrame = movementSpeed * Time.deltaTime;
        if (dir.magnitude <= distanceThisFrame)
        {
            Attack();
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        //transform.LookAt(target.transform);
    }
    public virtual void GetTargets()
    {
        GameObject nearestOpponent = null;
        float shortestDistance = Mathf.Infinity;
        foreach (GameObject opponent in opponentList)
        {
            float distanceToOpponent = Vector2.Distance(this.transform.position, opponent.transform.position);
            if (this.transform.position.x > opponent.transform.position.x)
            {
                direction = 1.0f;
            }
            else
            {
                direction = -1.0f;
            }
            if (distanceToOpponent < shortestDistance)
            {
                shortestDistance = distanceToOpponent;
                nearestOpponent = opponent;
            }
            if (nearestOpponent != null && shortestDistance <= detectionRange)
            {
                target = nearestOpponent;
            }
            else
            {
                target = null;
            }
        }
    }
    void CheckTarget()
    {
        if (target == null) return;
        UnitsBase Target = target.GetComponent<UnitsBase>();
        if (targetType == TargetType.Ground && Target.unitType == UnitType.Ground)
        {
            print("Target is Ground Type and so am I");
        }

        // Unit can hit both types
        // Apply to Air Units and Paper unit (ground)
        if (targetType == TargetType.Both && (Target.unitType == UnitType.Air || Target.unitType == UnitType.Ground))
        {
            print("Target is Air or Ground but I can hit Everything");
        }
    }

    private void Start()
    {
        tempAttackRate = attackRate;
        gameController = GameController.gameControllerInstance;
        spawnerManager = SpawnerManager.spawnerManagerInstance;
    }
    void Update()
    {
        if (this.tag == "Friendly")
        {
            if (health <= 0)
            {
                foreach (GameObject enemyUnit in spawnerManager.spawnedEnemiesList)
                {
                    enemyUnit.GetComponent<UnitsBase>().opponentList.Remove(this.gameObject);
                }
                
            }

        }
        if (this.tag == "Enemy" || this.tag == "Boss")
        {
            if (health <= 0)
            {
                foreach(GameObject friendlyUnit  in spawnerManager.spawnedFriendlyList)
                {
                    friendlyUnit.GetComponent<UnitsBase>().opponentList.Remove(this.gameObject);
                }
                spawnerManager.spawnedEnemiesList.Remove(this.gameObject);
                gameController.gold += goldAmount;
            }

        }
        if (health <= 0)
        {
            BuildManager.buildManagerInstance.curEnergyBank -= this.energyCost;
            Destroy(this.gameObject);
        }
        GetTargets();
        //CheckTarget();
        GoToTarget();
        if (target == null)
        {
            unitState = UnitState.Moving;
        }

        if (attackRate >= 0)
        {
            attackRate -= Time.deltaTime;
        }

    }
    public virtual void Attack()
    {
        if (target == null)
        {
            unitState = UnitState.Moving;
        }
        if (attackRate <= 0)
        {
            unitState = UnitState.Attacking;
            target.GetComponent<UnitsBase>().TakeDamage(damage);
            attackRate = tempAttackRate;
        }
        else
        {
            return;
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
}
