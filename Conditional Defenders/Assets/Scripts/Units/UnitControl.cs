﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitControl : MonoBehaviour
{
    public GameObject unitToControl;
    public Vector3 targetPosition;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.transform == null) return;
            if (hit.transform.gameObject.tag == "Friendly")
            {
                
                //if (SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Exists(x => hit.transform.gameObject)) return;
                unitToControl = hit.transform.gameObject;
                targetPosition = unitToControl.transform.position;
            }

        }

        if (Input.GetMouseButton(1))
        {
            if (unitToControl == null) return;
            targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPosition.z = unitToControl.transform.position.z;
        }
        if (unitToControl == null) return;

        unitToControl.transform.position = Vector3.MoveTowards(unitToControl.transform.position, targetPosition, unitToControl.GetComponent<UnitsBase>().movementSpeed * Time.deltaTime);


    }
}
