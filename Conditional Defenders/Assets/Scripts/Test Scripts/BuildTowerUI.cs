﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildTowerUI : MonoBehaviour
{
    public List<Button> buttonsList;
    public bool isOpened;

    void Start()
    {
        for (int i = 0; i < buttonsList.Count; i++)
        {
            buttonsList[i].interactable = false;
            buttonsList[i].image.enabled = false;
        }
        isOpened = false;
    }
    public void OpenCloseUI()
    {
        if (!isOpened)
        {
            for (int i = 0; i < buttonsList.Count; i++)
            {
                buttonsList[i].interactable = true;
                buttonsList[i].image.enabled = true;
            }
            isOpened = true;
        }
        else if (isOpened)
        {
            for (int i = 0; i < buttonsList.Count; i++)
            {
                buttonsList[i].interactable = false;
                buttonsList[i].image.enabled = false;
            }
            isOpened = false;
        }
    }
}
