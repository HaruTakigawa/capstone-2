﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTileController : MonoBehaviour
{
    public Color hoverColor;

    private Renderer rend;
    private Color originalColor;
    public List<GameObject> buildTileList;
    public bool isOpened;

    void Start()
    {
        rend = GetComponent<Renderer>();
        originalColor = rend.material.color;
        for (int i = 0; i < buildTileList.Count; i++)
        {
            buildTileList[i].SetActive(false);
        }
    }
    void OnMouseEnter()
    {
        rend.material.color = hoverColor;
    }
    void OnMouseExit()
    {
        rend.material.color = originalColor;
    }
    public void OnMouseDown()
    {
        if (!isOpened)
        {
            for (int i = 0; i < buildTileList.Count; i++)
            {
                buildTileList[i].SetActive(true);
            }
            isOpened = true;
        }
        else if (isOpened)
        {
            for (int i = 0; i < buildTileList.Count; i++)
            {
                buildTileList[i].SetActive(false);
            }
            isOpened = false;
        }
    }
}
