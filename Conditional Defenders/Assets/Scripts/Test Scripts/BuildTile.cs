﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTile : MonoBehaviour
{
    public GameObject BuildTileController;
    private GameObject turret;
    public GameObject turretPrefab;
    BuildManager buildManager;
    private void Start()
    {
        buildManager = BuildManager.buildManagerInstance;
    }
    void OnMouseDown()
    {
        if (turret != null)
        {
            //Sell or upgrade
        }
        if (turret == null)
        {


            BuildManager.buildManagerInstance.SetTurretToBuild(turretPrefab);
            GameObject turretToBuild = BuildManager.buildManagerInstance.GetTurretToBuild();
            //turret = (GameObject)Instantiate(turretToBuild,this.gameObject.GetComponentInParent<Transform>().transform.position,this.gameObject.GetComponentInParent<Transform>().transform.rotation);

            if (turretToBuild.GetComponent<TowerBase>().goldCost > GameController.gameControllerInstance.gold)
            {

                print("Not enough gold");
                turretToBuild = null;
                return;
            }
            else
            {
                GameController.gameControllerInstance.gold -= turretToBuild.GetComponent<TowerBase>().goldCost;
                buildManager.maxEnergyBank += turretToBuild.GetComponent<TowerBase>().energy;
                turret = (GameObject)Instantiate(turretToBuild, transform.position, transform.rotation);
                turret.transform.position = BuildTileController.transform.position;
                turret.transform.SetParent(BuildTileController.transform);
                buildManager.towerList.Add(turret);
                this.GetComponentInParent<BuildTileController>().OnMouseDown();
                this.GetComponentInParent<SpriteRenderer>().enabled = false;
                this.GetComponentInParent<BoxCollider2D>().enabled = false;
            }
        }
    }
}

