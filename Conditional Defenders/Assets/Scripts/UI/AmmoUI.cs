﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AmmoUI : MonoBehaviour
{
    public Image ammoBar;
    public Text ammoText;
    public PointAndShoot ammo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ammoBar.fillAmount = ammo.currentAmmo / ammo.maxAmmo;

        if (ammo.isReloading == true)
        {
            ammoText.text = "Reloading";
        }

        else
        {
            ammoText.text = "Ammo";
        }
    }
}
