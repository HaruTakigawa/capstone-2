﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DeductHealthUI : MonoBehaviour
{
    GameController gameController;
    
    private void Start()
    {
        gameController = GameController.gameControllerInstance;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            print("Pumasok");
            gameController.currentPollution += gameController.addedPollution;
            SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Remove(col.gameObject);
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "Boss")
        {
            SceneManager.LoadScene("BossFight");
        }

        if (gameController.currentPollution >= gameController.maxPollution)
        {
            gameController.currentPollution = gameController.maxPollution;
        }
    }
}
