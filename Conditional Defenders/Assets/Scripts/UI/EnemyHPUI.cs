﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHPUI : MonoBehaviour
{
    private float fill;
    public Image fillImage;
    private float maxHP;
    private void Start()
    {
        maxHP = this.GetComponentInParent<UnitsBase>().health;
    }
    private void Update()
    {
        fill = this.GetComponentInParent<UnitsBase>().health / maxHP;
        fillImage.fillAmount = fill;
    }
}
