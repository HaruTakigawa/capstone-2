﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerHPUI : MonoBehaviour
{
    public Health playerHealth;
    public Timer timer;
    public List<GameObject> hearts;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void RemoveHeart()
    {
        hearts[hearts.Count - 1].SetActive(false);
        hearts.RemoveAt(hearts.Count - 1);
    }
}
