﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text maxEnergy;
    public Text curEnergy;
    public Text currentGold;
    public Text waveNumber;
    public Image pollutionBar;
    public GameController gameController;
    public BuildManager buildManager;
    public SpawnerManager spawnerManager;
    private void Start()
    {
        gameController = GameController.gameControllerInstance;
        buildManager = BuildManager.buildManagerInstance;
        spawnerManager = SpawnerManager.spawnerManagerInstance;
    }
    void Update()
    {
        currentGold.text = gameController.gold.ToString();
        maxEnergy.text = buildManager.maxEnergyBank.ToString();
        curEnergy.text = buildManager.curEnergyBank.ToString();
        waveNumber.text = spawnerManager.currentWave.ToString();
        //waveNumber.text = gameController.spawner.waves.ToString();
        // Bar type
        pollutionBar.fillAmount = gameController.currentPollution/gameController.maxPollution;
        // Count Type
        // healthText.text = gameController.lives.ToString();
    }
}
