﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebuffBase : MonoBehaviour
{
    
    public float duration = 10f;
    
    public float timer;
    // Use this for initialization
    protected virtual void Update()
    {
        timer += Time.deltaTime;
        
        if (timer >= duration)
        {
            Destroy(this);
        }
    }
    public virtual void Effect()
    {

    }
}
