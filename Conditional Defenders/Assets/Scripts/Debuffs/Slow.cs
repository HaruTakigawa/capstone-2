﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow : DebuffBase
{
    UnitsBase unitsBase;
    private float slowAmount = 2;
    private float enemMovementTemp;
    // Use this for initialization
    private void Start()
    {
        unitsBase = GetComponent<UnitsBase>();
        enemMovementTemp = unitsBase.movementSpeed;
        Effect();
    }
    protected override void Update()
    {
        if (unitsBase == null)
        {
            Destroy(this);
        }
        timer += Time.deltaTime;

        if (timer >= duration)
        {
            unitsBase.movementSpeed = enemMovementTemp;
            Destroy(this);
        }
    }
    public override void Effect()
    {
        if (unitsBase != null)
            unitsBase.movementSpeed = unitsBase.movementSpeed / slowAmount;
    }
}
