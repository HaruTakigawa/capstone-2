﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EnemyMovement : MonoBehaviour
{
    UnitsBase unitsBase;
    private Transform target;
    private int wavepointIndex = 0;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        unitsBase = this.GetComponent<UnitsBase>();
        target = Waypoints.points[0];
        gameController = GameController.gameControllerInstance;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space)) SceneManager.LoadScene("BossFight");
        if(this.GetComponent<UnitsBase>().unitState == UnitsBase.UnitState.Attacking)
        {
            return;
        }
        Vector3 direction = target.position - transform.position;
        transform.Translate(direction.normalized * unitsBase.movementSpeed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            GetNextWaypoint();
        }
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.points.Length - 1)
        {
            if(this.tag == "Boss")
            {
                SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Remove(this.gameObject);
                SceneManager.LoadScene("BossFight");
            }
            SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Remove(this.gameObject);
            gameController.currentPollution += gameController.addedPollution;
            Destroy(gameObject);
            
            return;
        }

        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];
    }
}
