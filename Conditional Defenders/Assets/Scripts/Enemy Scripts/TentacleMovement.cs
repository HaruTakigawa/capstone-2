﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleMovement : MonoBehaviour
{
    public float speed = 2.5f;
    public float xMovement;
    public float yMovement;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.PingPong(Time.time * speed, xMovement), Mathf.PingPong(Time.time * speed, yMovement), transform.position.z);
    }
}
