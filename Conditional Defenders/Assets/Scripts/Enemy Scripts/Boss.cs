﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Boss : MonoBehaviour
{
    public Health health;
    public Image healthBar;
    public Image wpHealthBar;
    public Health weakPointHealth;
    //public float damageCooldown;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(DamagePlayer());
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.fillAmount = health.currentHealth / health.maxHealth;
        wpHealthBar.fillAmount = weakPointHealth.currentHealth / weakPointHealth.maxHealth;
        if (health.currentHealth <= 0)
        {
            Destroy(gameObject);
            healthBar.enabled = false;
            wpHealthBar.enabled = false;
            SceneManager.LoadScene("MainMenu");
            Cursor.visible = true;
        }

        if (weakPointHealth.currentHealth <= 0f)
        {
            health.currentHealth -= 100;
            weakPointHealth.currentHealth = weakPointHealth.maxHealth;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Crosshair")
        {
            Debug.Log("Colliding");
        }
    }

    //public IEnumerator DamagePlayer()
    //{
    //    while (true)
    //    {
    //        yield return new WaitForSeconds(damageCooldown);
    //        PlayerHealth.currentHealth -= 20;
    //        yield return new WaitForSeconds(damageCooldown);

    //    }
    //}
}
